"""
=======================================================
Comparison of LDA and PCA 2D projection of Iris dataset
=======================================================

The Iris dataset represents 3 kind of Iris flowers (Setosa, Versicolour
and Virginica) with 4 attributes: sepal length, sepal width, petal length
and petal width.

Principal Component Analysis (PCA) applied to this data identifies the
combination of attributes (principal components, or directions in the
feature space) that account for the most variance in the data. Here we
plot the different samples on the 2 first principal components.

Linear Discriminant Analysis (LDA) tries to identify attributes that
account for the most variance *between classes*. In particular,
LDA, in contrast to PCA, is a supervised method, using known class labels.
"""
print(__doc__)

import matplotlib.pyplot as plt

from sklearn import datasets
from sklearn.decomposition import PCA
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis

iris = datasets.load_iris()

X = iris.data
y = iris.target

print 'iris'
print iris




print y
print '___'

target_names = iris.target_names

pca = PCA(n_components=2)
X_r = pca.fit(X).transform(X)
print X_r

print '____'


print X_r[True, 0]
print X_r[False, 0]




# Percentage of variance explained for each components
print('explained variance ratio (first two components): %s'
      % str(pca.explained_variance_ratio_))

# plt.figure()
# for c, i, target_name in zip("rgb", [0, 1, 2], target_names):
#     plt.scatter(X_r[y == i, 0], X_r[y == i, 1], c=c, label=target_name)
# plt.legend()
# plt.title('PCA of IRIS dataset')



for c, i, target_name in zip("rgb", [0, 1, 2], target_names):
	print c,'-',i,'-',target_name

 
